//
//  DogYearsUnitTests.swift
//  DogYearsUnitTests
//
//  Created by Paulo Rosa on 24/01/18.
//  Copyright © 2018 Razeware. All rights reserved.
//

import XCTest
@testable import DogYears

class DogYearsUnitTests: XCTestCase {
    
    let calc = Calculator()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAdd() {
        let result = calc.evaluate(op: "+", arg1: 2.0, arg2: 2.0)
        XCTAssert(result == 4.0, "Calculator add operation Failed")
    }
    
    func testSubtractions() {
        let result = calc.evaluate(op: OperationType.subtract.rawValue, arg1: 2.0, arg2: 2.0)
        XCTAssert(result == 0.0, "Calculator subs operation failed")
    }
}
